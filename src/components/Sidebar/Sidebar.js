import React from "react";
import "./sidebar.css";
import aboutMe from "../../img/aboutMe.png";
import competenceAcademique from "../../img/competenceAcademique.png";
import contact from "../../img/contact.png";
import skills from "../../img/skills.png";

export default function Sidebar({ scrollCallback }) {
  const handleClickSideBar = (mode) => {
    scrollCallback(mode);
  };

  const handleClick = () => {
    const line1 = document.querySelector(".first");
    const line2 = document.querySelector(".second");
    const line3 = document.querySelector(".third");
    const toggle = document.querySelector(".toggle");
    const element1 = document.querySelector(".element1");
    const element2 = document.querySelector(".element2");
    const element3 = document.querySelector(".element3");
    const element4 = document.querySelector(".element4");

    toggle.classList.toggle("active");
    line1.classList.toggle("active");
    line2.classList.toggle("active");
    line3.classList.toggle("active");
    element1.classList.toggle("active");
    element2.classList.toggle("active");
    element3.classList.toggle("active");
    element4.classList.toggle("active");
  };

  window.addEventListener("click", (e) => {
    const line1 = document.querySelector(".first");
    const line2 = document.querySelector(".second");
    const line3 = document.querySelector(".third");
    const toggle = document.querySelector(".toggle");

    const element1 = document.querySelector(".element1");
    const element2 = document.querySelector(".element2");
    const element3 = document.querySelector(".element3");
    const element4 = document.querySelector(".element4");

    if (
      !e.target.matches(".toggle") &&
      !e.target.matches(".photo1") &&
      !e.target.matches(".photo2") &&
      !e.target.matches(".photo3") &&
      !e.target.matches(".photo4")
    ) {
      toggle.classList.remove("active");
      line1.classList.remove("active");
      line2.classList.remove("active");
      line3.classList.remove("active");

      element1.classList.remove("active");
      element2.classList.remove("active");
      element3.classList.remove("active");
      element4.classList.remove("active");
    }
  });

  return (
    <div className="sideBar">
      <ul className="toggle" onClick={handleClick}>
        <li className="first"></li>
        <li className="second"></li>
        <li className="third"></li>
      </ul>
      <div className="elements_sideBar">
        <div className="element1">
          <img
            className="photo1"
            src={aboutMe}
            alt="aboutMe"
            onClick={() => handleClickSideBar("aboutMe")}
          />
        </div>
        <div className="element2">
          <img
            className="photo2"
            src={skills}
            alt="Skills"
            onClick={() => handleClickSideBar("competence")}
          />
        </div>
        <div className="element3">
          <img
            className="photo3"
            src={competenceAcademique}
            alt="projects"
            onClick={() => handleClickSideBar("project")}
          />
        </div>
        <div className="element4">
          <img
            className="photo4"
            src={contact}
            alt="contact"
            onClick={() => handleClickSideBar("contact")}
          />
        </div>
      </div>
    </div>
  );
}
