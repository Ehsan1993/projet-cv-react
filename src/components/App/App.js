import React, { useState, useRef } from "react";
import PartiePrincipale from "../PartiePrincipale/PartiePrincipale.js";
import Photo from "../Photo/Photo.js";
import Sidebar from "../Sidebar/Sidebar.js";
import "./App.css";
// import { Route, Routes } from "react-router-dom";
// import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {
  const aboutRef = useRef();
  // const [scroll, scrollCallback] = useState("");
  // console.log(scroll);

  const scrollCallback = () => {
    aboutRef.current.scrollIntoView({ behavior: "smooth" });
    console.log(aboutRef.current.id);
    console.log(scrollCallback.name);
  };

  return (
    // <div className="app">
    //   <Routes>
    //     <Route path="/" element={<Sidebar className="sideBar" />} />
    //     <Route path="/" element={<Photo className="photoContainer" />} />
    //     <Route
    //       path="/"
    //       element={<PartiePrincipale className="partiePrincipaleContainer" />}
    //     />
    //   </Routes>
    <div className="app">
      {/* <Sidebar scrollCallback={scrollCallback} className="sideBar" /> */}
      <Photo className="photoContainer" />
      <PartiePrincipale
        refProps={aboutRef}
        className="partiePrincipaleContainer"
      />
    </div>
    // </div>
  );
}

export default App;
