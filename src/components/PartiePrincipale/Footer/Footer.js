import "./Footer.css";
import cv from "./../../../img/pdf-cv.png";

export default function Footer() {
  return (
    <div className="footerContainer">
      <div className="contents">
        <div className="CV">
          <label>CV en format pdf</label>
          <a
            className="cvLink"
            href="https://drive.google.com/file/d/1qIUBJhL7DbX6EicEu_E_9DW7Rg1nW88n/view?usp=share_link"
            target="-_blank"
          >
            <img src={cv} alt="CV-pdf" />
          </a>
        </div>
      </div>
    </div>
  );
}
