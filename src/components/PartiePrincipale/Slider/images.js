import javaFx from "../../../img/javaFx.PNG";
import swing from "../../../img/swing.PNG";
import cv from "../../../img/cv.PNG";
import formulaire from "../../../img/formulaireAbsence.PNG";
import ecole from "../../../img/ecoleMusique.PNG";
import cvReact from "../../../img/cv_react.PNG";

export default [
  {
    id: 1,
    image: swing,
    title: "Java - Gestion de pharmacie",
    Interface: "Java swing",
    bdd: "non-connecté",
    lien: "https://gitlab.com/Ehsan1993/cf",
  },
  {
    id: 2,
    image: javaFx,
    title: "Java - Gestion de pharmacie",
    Interface: "JavaFX",
    bdd: "MySQL",
    lien: "https://gitlab.com/Ehsan1993/ecf-sparadrap_javafx",
  },
  {
    id: 3,
    image: cv,
    title: "HTML/CSS/JS - CV",
    Interface: "web",
    bdd: "non-connecté",
    lien: "https://ehsanbayat.surge.sh",
  },
  {
    id: 4,
    image: formulaire,
    title: "HTML/CSS/JS - Formulaire absence",
    Interface: "web",
    bdd: "fichier JSON",
    lien: "https://gitlab.com/Ehsan1993/ecf-formulaireautorisationabsence",
  },
  {
    id: 5,
    image: cvReact,
    title: "React js - CV",
    Interface: "web",
    bdd: "non-connecté",
    lien: "https://gitlab.com/Ehsan1993/projet-cv-react",
  },
  {
    id: 6,
    image: ecole,
    title: "JEE - Gestion école de musique",
    Interface: "JEE web services",
    bdd: "MySQL",
    lien: "https://gitlab.com/Ehsan1993/javaee-ecole-de-musique",
  },
];
