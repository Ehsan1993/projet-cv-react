import React from "react";
import "./Slider.css";
import images from "./images.js";

import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

function Slider() {
  return (
    <div className="sliderContainer">
      <Carousel
        autoPlay
        infiniteLoop
        useKeyboardArrows
        showStatus={false}
        showThumbs={false}
        stopOnHover
        className="carousel"
      >
        {images.map((image) => {
          return (
            <div className="item" key={image.id}>
              <div className="textesContainer">
                <label>{image.title}</label>
                <label>Interface: {image.Interface}</label>
                <label>Base de donnée: {image.bdd}</label>
              </div>
              <a href={image.lien} target="_blank" rel="noreferrer">
                <img src={image.image} alt="" />
              </a>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}

export default Slider;
