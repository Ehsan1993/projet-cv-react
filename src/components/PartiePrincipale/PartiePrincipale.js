import React from "react";
import Slider from "./Slider/Slider.js";
import "./partiePrincipale.css";
import developpement from "../../img/developpement.png";
import dataBase from "../../img/dataBase.png";
import logiciel from "../../img/logiciel.png";
import langues from "../../img/langues.png";
import PartieFooter from "./Footer/Footer.js";
import PartieContact from "./Contact/Contact.js";

export default function PartiePrincipale({ refProps }) {
  return (
    <div className="partiePrincipaleContainer">
      <div className="partiePrincipale">
        <h1 id="aboutMe" className="titrePartiePrincipale" ref={refProps}>
          About me
        </h1>
        <div className="sous-titre">
          <h4>30 ans</h4>
          <h4 className="slush">/</h4>
          <h4>Concepteur Développeur Informatique</h4>
        </div>
        <h4 className="description">
          Master II (bac + 5) en Relations Internationales & Diplomatie, avec
          plus de 3 ans d'expériences dans le domaine. Passionné en
          informatique, j'ai appris à coder pendant ma formation "Titre
          Professionnel Concepteur Développeur d'Application" dans divers
          langages (Java, JavaScript ...) et ai créé des projets personnels.
          Autonome sur mon poste tout en appréciant le travail en équipe avec
          des collaborateurs sur site ou distants, je cherche un poste
          concepteur et/ou développeur informatique en langage Java ou C# sur
          Nancy ou alentour.
        </h4>
        <h2 id="competence" className="styleSousTitre" ref={refProps}>
          Compétences
        </h2>
        <div className="boxContainer">
          <div className="boxes">
            <img className="img-dev" src={developpement} alt="Developpement" />
            <h3>Développement</h3>
            <ul className="liste listeDev">
              <li>JAVA</li>
              <li>C#</li>
              <li>React</li>
              <li>Angular</li>
              <li>Bootstrap</li>
              <li>Java Script</li>
              <li>HTML 5</li>
              <li>CSS 3</li>
            </ul>
          </div>
          <div className="boxes">
            <img className="img-dev" src={dataBase} alt="dataBase" />
            <h3>Base de donnée</h3>
            <ul className="liste">
              <li>SQL (MySQL, SQL Server)</li>
              <li>NoSQL</li>
            </ul>
          </div>
          <div className="boxes">
            <img className="img-dev" src={logiciel} alt="Developpement" />
            <h3>Logiciel</h3>
            <ul className="liste">
              <li>Microsoft Office (Word, PowerPoint, Excel)</li>
              <li>Photoshop</li>
            </ul>
          </div>
          <div className="boxes">
            <img className="img-dev langue" src={langues} alt="Langues" />
            <h3>Langues</h3>
            <ul className="liste">
              <li>Persan/Dari</li>
              <li>Français</li>
              <li>Anglais</li>
            </ul>
          </div>
        </div>
        <h2 id="project" className="styleSousTitre">
          Projets
        </h2>
        <Slider className="slider" />
        <PartieContact id="contact" />
        <PartieFooter />
      </div>
    </div>
  );
}
