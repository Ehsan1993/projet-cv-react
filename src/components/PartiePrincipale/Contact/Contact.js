import "./Contact.css";

export default function Contact() {
  return (
    <div className="contactContainer">
      <form
        action="https://formsubmit.co/7bf79177712ccb4a42ba8a258af7b959"
        method="POST"
      >
        <div className="contactLabel">Contact</div>
        <input placeholder="Nom/Prénom" type="text" name="lastName" required />
        <input placeholder="Adresse mail" type="email" name="email" required />
        <input placeholder="Sujet..." name="_subject" />
        <input type="hidden" name="_captcha" value="false" />
        <input
          type="hidden"
          name="_next"
          value="https://ehsan-react-cv.surge.sh/"
        />
        <textarea
          placeholder="Ecrivez votre message ici..."
          rows="3"
          name="message"
          required
        />
        <button action="submit">Envoyer</button>
      </form>
    </div>
  );
}
