import React from "react";
import photoEhsan from "../../img/photoEhsan.png";
import share from "../../img/share4.png";
import linkedIn from "../../img/linkedIn.png";
import facebook from "../../img/facebook.png";
import instagram from "../../img/instagram.png";
import gmail from "../../img/gmail.png";
import gitlab from "../../img/gitlab.png";
import "./photo.css";

export default function Photo() {
  const handleClick = () => {
    const btn_1 = document.querySelector(".linkedIn");
    const btn_2 = document.querySelector(".facebook");
    const btn_3 = document.querySelector(".instagram");
    const btn_4 = document.querySelector(".gmail");
    const btn_5 = document.querySelector(".gitlab");
    const s = document.querySelector(".socialMedia");
    btn_1.classList.toggle("active");
    btn_2.classList.toggle("active");
    btn_3.classList.toggle("active");
    btn_4.classList.toggle("active");
    btn_5.classList.toggle("active");
    s.classList.toggle("active");
  };

  window.addEventListener("click", (e) => {
    const btn_1 = document.querySelector(".linkedIn");
    const btn_2 = document.querySelector(".facebook");
    const btn_3 = document.querySelector(".instagram");
    const btn_4 = document.querySelector(".gmail");
    const btn_5 = document.querySelector(".gitlab");
    const s = document.querySelector(".socialMedia");

    if (!e.target.matches(".image")) {
      btn_1.classList.remove("active");
      btn_2.classList.remove("active");
      btn_3.classList.remove("active");
      btn_4.classList.remove("active");
      btn_5.classList.remove("active");
      s.classList.remove("active");
    }
  });

  return (
    <div className="photoContainer">
      <div className="content">
        <img className="photo_ehsan" src={photoEhsan} alt="Ehsan" />
        <div className="texteContainer">
          <h1 className="name">Mohammad Ehsan</h1>
          <h1 className="last_name">BAYAT</h1>
          <h1 className="texteLabel">Développeur</h1>
        </div>
        <div className="socialMedia_container">
          <div className="socialMedia">
            <img
              onClick={handleClick}
              className="image"
              src={share}
              alt="Social Media"
            />
          </div>
          <ul className="btn-mediaContainer">
            <li className="linkedIn">
              <a
                href="https://www.linkedin.com/in/ehsan-bayat-34765062/"
                target="_blank"
                rel="noreferrer"
              >
                <img className="media" src={linkedIn} alt="LinkedIn" />
              </a>
            </li>
            <li className="facebook">
              <a
                href="https://www.facebook.com/ehsan.bayat.10"
                target="_blank"
                rel="noreferrer"
              >
                <img className="media" src={facebook} alt="Facebook" />
              </a>
            </li>
            <li className="instagram">
              <a
                href="https://www.instagram.com/ali.moh2019/"
                target="_blank"
                rel="noreferrer"
              >
                <img className="media" src={instagram} alt="Instagram" />
              </a>
            </li>
            <li className="gmail">
              <a
                href="mailto:m.ehsanbayat@gmail.com"
                target="_blank"
                rel="noreferrer"
              >
                <img className="media" src={gmail} alt="Gmail" />
              </a>
            </li>
            <li className="gitlab">
              <a
                href="https://gitlab.com/Ehsan1993"
                target="_blank"
                rel="noreferrer"
              >
                <img className="media" src={gitlab} alt="GitLab" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
